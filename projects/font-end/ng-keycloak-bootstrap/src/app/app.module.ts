import { BrowserModule } from '@angular/platform-browser';
import { NgModule, DoBootstrap } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ClarityModule } from '@clr/angular';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';

import { AppComponent, HomeComponent } from './components';
import { HeroesService } from './services';
import { AppRoutingModule } from './app-routing.module';

import { environment } from '../environments/environment';

// configuring providers
import { ApiModule, Configuration, ConfigurationParameters } from './pet-module';
import { PetsComponent } from './components/pets/pets.component';

export function apiConfigFactory (): Configuration {
  const params: ConfigurationParameters = {
    // set configuration parameters here.
  };
  return new Configuration(params);
}

const keycloakService: KeycloakService = new KeycloakService();

@NgModule({
  declarations: [AppComponent, HomeComponent, PetsComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    ClarityModule,
    KeycloakAngularModule,
    AppRoutingModule,
    ApiModule.forRoot(apiConfigFactory)
  ],
  providers: [
    HeroesService,
    {
      provide: KeycloakService,
      useValue: keycloakService
    }
  ],
  entryComponents: [AppComponent]
})
export class AppModule implements DoBootstrap {
  async ngDoBootstrap(app) {
    const { keycloakConfig } = environment;

    try {
      await keycloakService.init({ config: keycloakConfig });
      app.bootstrap(AppComponent);
    } catch (error) {
      console.error('Keycloak init failed', error);
    }
  }
}
