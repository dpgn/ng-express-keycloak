import { Component, OnInit, AfterViewInit } from '@angular/core';

import { map } from 'rxjs/operators';

import { pathValues } from '../../utils';
import { Observable } from 'rxjs';
import { PetService, Pet } from 'app/pet-module';

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.css']
})
export class PetsComponent {
  pets$: Observable<Pet[]>;

  constructor(private petService: PetService) {
    this.listPets();
  }

  listPets(): void {
    this.pets$ = this.petService.findPetsByStatus(['available']);
  }
}
