'use strict';

var jwt = require('jsonwebtoken');
var sharedSecret = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAj/rpmcTCwsAm8x4IMAeD
+TQdlLXNAapcHioWPl3M6Rdv+OU8E1SiICdFPq644jE+pvgJgm9Knsxd5uchEChF
7lf6BkGS3vwWYfd33DQ3GjgFiKh+Lj2oLfYPKBdhSNn186YItC6XVyjctq31XAwb
ORpDUGo9zz3UOpOUueND/fwB2yxeX9qJJW49eTWRYHyuE2oRM6xryUltZ0CKMDdC
7fVW+R3f2A3tlMaydCK3Dn2YOOsIVNia3gxoEw6sFFdGIHk7ACRx+9P20IqybARf
Jv65eXltQr04OoTjAr/rKmVHcuGzakhXfaTAFHsQmk4RlxGhBdXV/Fie1bDHm08K
WwIDAQAB
-----END PUBLIC KEY-----
`;
var issuer = 'http://localhost:8080/auth/realms/dpgn';

//Here we setup the security checks for the endpoints
//that need it (in our case, only /protected). This
//function will be called every time a request to a protected
//endpoint is received
exports.verifyToken = function (req, authOrSecDef, token, callback) {
    //these are the scopes/roles defined for the current endpoint
    //console.log(`req : ${req}`);
    //console.log(`authOrSecDef : ${JSON.stringify(authOrSecDef)}`);
    // console.log(`token : ${token}`);
    // console.log(`callback : ${callback}`);

    // const arr = Object.keys(req);
    // const sss = arr.join(' ,');
    // console.log(`keys : ${sss}`);

    function sendError() {
        //return req.res.status(403).json({message: 'Error: Access Denied'});
        throw new Error('Error: Access Denied');
    }

    //validate the 'Authorization' header. it should have the following format:
    //'Bearer tokenString'
    if (token && token.indexOf("bearer ") == 0) {

        var tokenString = token.split(' ')[1];

        jwt.verify(tokenString, sharedSecret, {algorithms: ["RS256"]}, function (verificationError, decodedToken) {
            console.log(`verificationError : ${verificationError}`);
            //console.log(`decodedToken : ${JSON.stringify(decodedToken)}`);
            //check if the JWT was verified correctly
            if (verificationError == null 
                && decodedToken 
                && decodedToken.resource_access
                && decodedToken.resource_access['pet-auth']
                && decodedToken.resource_access['pet-auth'].roles
                && Array.isArray(decodedToken.resource_access['pet-auth'].roles)
                && decodedToken.resource_access['pet-auth'].roles.length
                ) {
                // check if the role is valid for this endpoint
                var roleMatch = ['role-pet-admin'].indexOf(decodedToken.resource_access['pet-auth'].roles[0]) !== -1;
                // check if the issuer matches
                var issuerMatch = decodedToken.iss == issuer;

                // you can add more verification checks for the
                // token here if necessary, such as checking if
                // the username belongs to an active user

                if (roleMatch && issuerMatch) {
                    //add the token to the request so that we
                    //can access it in the endpoint code if necessary
                    req.auth = decodedToken;
                    //if there is no error, just return null in the callback
                    return callback(null);
                } else {
                    //return the error in the callback if there is one
                    return callback(sendError());
                }

            } else {
                //return the error in the callback if the JWT was not verified
                return callback(sendError());
            }
        });
    } else {
        //return the error in the callback if the Authorization header doesn't have the correct format
        return callback(sendError());
    }
};
